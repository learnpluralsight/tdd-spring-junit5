package com.quovadis.exservice.productservice.services.impl;

import com.quovadis.exservice.productservice.models.Product;
import com.quovadis.exservice.productservice.repository.ProductRepositoryImpl;
import com.quovadis.exservice.productservice.services.ProductService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ProductRepositoryImplTest {

    @Autowired
    private ProductService service;

    @MockBean
    private ProductRepositoryImpl repository;

    @Test
    @DisplayName("Test findById success")
    void findById() {
        // Setup our mock
        Product mockProduct = new Product(1, "Product Name", 10, 1);
        doReturn(Optional.of(mockProduct)).when(repository).findById(1);

        // Execute the service call
        Optional<Product> returnedProduct = service.findById(1);

        // Assert the response
        assertTrue(returnedProduct.isPresent(), "Product was not found");
        assertSame(returnedProduct.get(), mockProduct, "Product should be the same");
    }

    @Test
    @DisplayName("Test findById Not found")
    void testFindByIdNotFound() {
        // Setup our mock
        Product mockProduct = new Product(1, "Product Name", 10, 1);
        doReturn(Optional.empty()).when(repository).findById(1);

        // Execute the service call
        Optional<Product> returnedProduct = service.findById(1);

        // Assert the response
        assertFalse(returnedProduct.isPresent(), "Product was found, when it shouldn't be");
    }

    @Test
    void getAll() {
    }

    @Test
    void delete() {
    }
}