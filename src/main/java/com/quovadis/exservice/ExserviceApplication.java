package com.quovadis.exservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExserviceApplication.class, args);
	}

}
