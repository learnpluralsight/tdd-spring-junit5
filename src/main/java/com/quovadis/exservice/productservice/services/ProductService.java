package com.quovadis.exservice.productservice.services;

import com.quovadis.exservice.productservice.models.Product;

import java.util.Optional;

public interface ProductService {
    Optional<Product> findById(Integer id);

    Iterable<Product> getAll();

    Boolean delete(Integer id);
}
