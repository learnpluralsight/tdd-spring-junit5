package com.quovadis.exservice.productservice.services.impl;

import com.quovadis.exservice.productservice.models.Product;
import com.quovadis.exservice.productservice.repository.ProductRepositoryImpl;
import com.quovadis.exservice.productservice.services.ProductService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepositoryImpl productRepositoryImpl;

    public ProductServiceImpl(ProductRepositoryImpl productRepositoryImpl){
        this.productRepositoryImpl = productRepositoryImpl;
    }

    @Override
    public Optional<Product> findById(Integer id) {
        return productRepositoryImpl.findById(id);
    }

    @Override
    public Iterable<Product> getAll() {
        return null;
    }

    @Override
    public Boolean delete(Integer id) {
        return productRepositoryImpl.delete(id);
    }
}
