package com.quovadis.exservice.productservice.repository;

import com.quovadis.exservice.productservice.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {

    Optional<Product> findById(Integer id);

    List<Product> findAll();

    Boolean update(Product product);

    Product save(Product product);

    Boolean delete(Integer id);
}
